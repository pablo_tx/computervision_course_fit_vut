# coding: utf-8
from __future__ import print_function

import numpy as np
import cv2

# * Indexing numpy arrays http://scipy-cookbook.readthedocs.io/items/Indexing.html

def parseArguments():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--video', help='Input video file name.')
    parser.add_argument('-i', '--image', help='Input image file name.')
    args = parser.parse_args()
    return args


def image(imageFileName):
    # read image
    img = cv2.imread(imageFileName)
    if img is None:
        print("Error: Unable to read image file", imageFileName)
        exit(-1)

    # print image width, height, and channel count
    print("Image dimensions: ",
          img.shape[1], "x", img.shape[0], "\nChannel count: ", img.shape[2])

    # Resize to width 400 and height 500 with bicubic interpolation.
    img = cv2.resize(img, (400, 500), interpolation=cv2.INTER_CUBIC)

    # Print mean image color and standard deviation of each color channel
    mean, stddev = cv2.meanStdDev(img)
    print('Image mean and standard deviation by channel: \n',
          "Mean R: ", mean[2], "\n",
          "Mean G: ", mean[1], "\n",
          "Mean B: ", mean[0], "\n",
          "STD R: ", stddev[2], "\n",
          "STD G: ", stddev[1], "\n",
          "STD B: ", stddev[0], "\n")

    # Fill horizontal rectangle with color 128.
    # Position x1=50,y1=120 and size width=200, height=50
    imgrect = img.copy()
    cv2.rectangle(imgrect, (50, 120), (250, 170), 128, -1)

    # write result to file
    cv2.imwrite('rectangle.png', imgrect)

    # Fill every third column in the top half of the image black.
    # The first column sould be black.
    # The rectangle should not be visible.
    halfHeight = int(img.shape[0]/2)
    blackCols = img[0:halfHeight, ::3] # Choose the correct rows
    blackCols[:] = 0 # Set them to black
        

    # write result to file
    cv2.imwrite('striped.png', img)  # FILL

    # Set all pixels with any collor lower than 100 black.
    ret, thr = cv2.threshold(img, 100, 0, cv2.THRESH_TOZERO)

    # write result to file
    cv2.imwrite('clip.png', thr)  # FILL


def video(videoFileName):
    # open video file and get basic information
    videoCapture = cv2.VideoCapture(videoFileName)
    frameRate = int(videoCapture.get(cv2.CAP_PROP_FPS))
    frame_width = int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    if not videoCapture.isOpened():
        print("Error: Unable to open video file for reading", videoFileName)
        exit(-1)

    # open video file for writing
    videoWriter = cv2.VideoWriter(
        'videoOut.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
        frameRate, (frame_width, frame_height))
    if not videoWriter.isOpened():
        print("Error: Unable to open video file for writing", videoFileName)
        exit(-1)

    while videoCapture.isOpened():
        ret, frame = videoCapture.read()
        if not ret:
            break

        # Flip image upside down.
        frame = cv2.flip(frame, 0)

        # Add white noise (normal distribution).
        # Standard deviation should be 5.
        # use np.random
        mean = 128.0   # white noise threshold
        std = 5.0    # standard deviatio
        noise = np.random.normal(mean, std, (frame.shape[0], frame.shape[1]))
        
        # Apply noise to all components
        if len(frame.shape) == 2:
            frame = frame + noise
        else:
            frame[:,:,0] = frame[:,:,0] + noise
            frame[:,:,1] = frame[:,:,1] + noise
            frame[:,:,2] = frame[:,:,2] + noise

        # Add gamma correction.
        # y = x^1.2 -- the image to the power of 1.2
        lookUpTable = np.empty((1,256), np.uint8)
        for i in range(256):
            lookUpTable[0,i] = np.clip(pow(i / 255.0, 1.2) * 255.0, 0, 255)
        frame = cv2.LUT(frame, lookUpTable)

        # Dim blue color to half intensity.
        frame[:,:,0] = frame[:,:,0] / 2

        # Invert colors.
        cv2.bitwise_not(frame,frame)

        # Display the processed frame.
        cv2.imshow("Output", frame)
        # Write the resulting frame to video file.
        videoWriter.write(frame)

        # End the processing on pressing Escape.
        # FILL
        cv2.waitKey(10)

    cv2.destroyAllWindows()
    videoCapture.release()
    videoWriter.release()


def main():
    args = parseArguments()
    np.random.seed(1)
    image(args.image)
    video(args.video)


if __name__ == "__main__":
    main()
